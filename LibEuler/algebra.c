#include "libeuler.h"

int8_t quadform(const double a, const double b, const double c,
        double * x1, double * x2)
{
    if (x1 == NULL || x2 == NULL) {
        return 0;
    }

    double tmp = (b*b) - (4*a*c);
    if (tmp < 0) {
        *x1 = 0;
        *x2 = 0;
        return 0;
    }

    tmp = sqrt(tmp);
    *x1 = ((-b) - tmp) / (2*a);
    *x2 = ((-b) + tmp) / (2*a);
    
    return 1 + (x1 != x2);
}

int8_t quadformf(const float a, const float b, const float c,
        float * x1, float * x2)
{
    if (x1 == NULL || x2 == NULL) {
        return 0;
    }

    float tmp = (b*b) - (4*a*c);
    if (tmp < 0) {
        *x1 = 0;
        *x2 = 0;
        return 0;
    }

    tmp = sqrtf(tmp);
    *x1 = ((-b) - tmp) / (2*a);
    *x2 = ((-b) + tmp) / (2*a);
    return 1 + (x1 != x2);
}

int8_t quadforml(const long double a, const long double b, const long double c,
        long double * x1, long double * x2)
{
    if (x1 == NULL || x2 == NULL) {
        return 0;
    }

    long double tmp = (b*b) - (4*a*c);
    if (tmp < 0) {
        *x1 = 0;
        *x2 = 0;
        return 0;
    }

    tmp = sqrtl(tmp);
    *x1 = ((-b) - tmp) / (2*a);
    *x2 = ((-b) + tmp) / (2*a);
    return 1 + (x1 != x2);
}
