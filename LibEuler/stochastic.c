#include "libeuler.h"

#include <math.h>
#include <stdio.h>


// means
// arithmetic mean
double mean(const double * const data, const size_t size)
{
    double mean = 0.0;
    for (size_t i = 0; i < size; i++) {
        mean += data[i];
    }
    mean /= (double)size;
    return mean;
}

float meanf(const float * const data, const size_t size)
{
    float mean = 0.0f;
    for (size_t i = 0; i < size; i++) {
        mean += data[i];
    }
    mean /= (float)size;
    return mean;
}

long double meanl(const long double * const data, const size_t size)
{
    long double mean = 0.0l;
    for (size_t i = 0; i < size; i++) {
        mean += data[i];
    }
    mean /= (long double)size;
    return mean;
}



// geometric mean
double gmean(const double * const data, const size_t size)
{
    double mean = 1.0;
    for (size_t i = 0; i < size; i++) {
        mean *= data[i];
    }
    mean = pow(mean, 1.0 / (double)size);
    return mean;
}

float gmeanf(const float * const data, const size_t size)
{
    float mean = 1.0f;
    for (size_t i = 0;i < size; i++) {
        mean *= data[i];
    }
    mean = powf(mean, 1.0f / (double)size);
    return mean;
}

long double gmeanl(const long double * const data, const size_t size)
{
    long double mean = 1.0l;
    for (size_t i = 0; i < size; i++) {
        mean *= data[i];
    }
    mean = powl(mean, 1.0l / (long double)size);
    return mean;
}


// harmonic mean
double hmean(const double * const data, const size_t size)
{
    double mean = 0.0;
    for (size_t i = 0; i < size; i++) {
        mean += 1.0 / data[i];
    }
    mean = (double)size / mean;
    return mean;
}

float hmeanf(const float * const data, const size_t size)
{
    float mean = 0.0f;
    for (size_t i = 0; i < size; i++) {
        mean += 1.0f / data[i];
    }
    mean = (float)size / mean;
    return mean;
}

long double hmeanl(const long double * const data, const size_t size)
{
    long double mean = 0.0l;
    for (size_t i = 0; i < size; i++) {
        mean += 1.0l / data[i];
    }
    mean = (long double)size / mean;
    return mean;
}



// median
double median(const double * const data, const size_t size)
{
    double median = 0.0;
    const size_t half_size = size / 2;
    if (size % 2 == 0) {
        median = (data[half_size-1] + data[half_size]) / 2.0;
    } else {
        median = data[half_size];
    }
    return median;
}

float medianf(const float * const data, const size_t size)
{
    float median = 0.0f;
    const size_t half_size = size / 2;
    if (size % 2 == 0) {
        median = (data[half_size-1] + data[half_size]) / 2.0f;
    } else {
        median = data[half_size];
    }
    return median;
}

long double medianl(const long double * const data, const size_t size)
{
    long double median = 0.0l;
    const size_t half_size = size / 2;
    if (size % 2 == 0) {
        median = (data[half_size-1] + data[half_size]) / 2.0l;
    } else {
        median = data[half_size];
    }
    return median;
}



// expected value
double expct(const double * const outcomes, 
        const double * const probabilities, 
        const size_t size)
{
    double e = 0.0;
    for (size_t i = 0; i < size; i++) {
        e += outcomes[i] * probabilities[i];
    }
    return e;
}

float expctf(const float * const outcomes, 
        const float * const probabilities, 
        const size_t size)
{
    float e = 0.0f;
    for (size_t i = 0; i < size; i++) {
        e += outcomes[i] * probabilities[i];
    }
    return e;
}

long double expctl(const long double * const outcomes, 
        const long double * const probabilities, 
        const size_t size)
{
    long double e = 0.0l;
    for (size_t i = 0; i < size; i++) {
        e += outcomes[i] * probabilities[i];
    }
    return e;
}


// variance

double var(const double * const outcomes, 
        const double * const probabilities, 
        size_t size)
{
    double e1 = 0.0;
    double e2 = 0.0;
    for (size_t i = 0; i < size; i++) {
        e1 += (outcomes[i] * outcomes[i]) * probabilities[i];
        e2 += outcomes[i] * probabilities[i];
    }
    double variance = e1 - e2 * e2;
    return variance;
}

float varf(const float * const outcomes, 
        const float * const probabilities, 
        size_t size)
{
    float e1 = 0.0f;
    float e2 = 0.0f;
    for (size_t i = 0; i < size; i++) {
        e1 += (outcomes[i] * outcomes[i]) * probabilities[i];
        e2 += outcomes[i] * probabilities[i];
    }
    float variance = e1 - e2 * e2;
    return variance;
    return 0;
}


long double varl(const long double * const outcomes, 
        const long double * const probabilities, 
        size_t size)
{
    long double e1 = 0.0l;
    long double e2 = 0.0l;
    for (size_t i = 0; i < size; i++) {
        e1 += (outcomes[i] * outcomes[i]) * probabilities[i];
        e2 += outcomes[i] * probabilities[i];
    }
    long double variance = e1 - e2 * e2;
    return variance;
}

// standard deviation

double stddev(const double * const outcomes, 
        const double * const probabilities, 
        size_t size)
{
    return sqrt(var(outcomes, probabilities, size));
}

float stddevf(const float * const outcomes, 
        const float * const probabilities, 
        size_t size)
{
    return sqrt(varf(outcomes, probabilities, size));
}

long double stddevl(const long double * const outcomes, 
        const long double * const probabilities, 
        size_t size)
{
    return sqrtl(varl(outcomes, probabilities, size));
}



















