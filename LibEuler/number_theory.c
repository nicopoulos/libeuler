#include "libeuler.h"

// sgn(x)
int8_t sgn(const double x)
{
    return (x > 0) - (x < 0);
}


int8_t sgnf(const float x)
{
    return (x > 0) - (x < 0);
}

int8_t sgnl(const long double x)
{
    return (x > 0) - (x < 0);
}

