#ifndef LIBEULER_H
#define LIBEULER_H

#include <stdlib.h>
#include <stdint.h>
#include <math.h>

/*
 * =================================================================
 *
 *                          CONSTANTS
 *
 * =================================================================
 */

// Pi
#define PI_L            3.14159265358979323846264338327950l // 33 significant digits
#define PI              3.14159265358979                    // 15 significant digits
#define PI_F            3.141592f                           // 7 significant digits

// Euler's number
#define E_L             2.71828182845904523536028747135266l
#define E               2.71828182845904
#define E_F             2.718281f

// golden ratio
#define PHI_L           1.61803398874989484820458683436563l
#define PHI             1.61803398874989
#define PHI_F           1.618033f

// irrational square roots
#define SQRT2_L         1.41421356237309504880168872420969l
#define SQRT2           1.41421356237309
#define SQRT2_F         1.414213f

#define SQRT3_L         1.73205080756887729352744634150587l
#define SQRT3           1.73205080756887
#define SQRT3_F         1.732050f

#define SQRT5_L         2.23606797749978969640917366873127l
#define SQRT5           2.23606797749978
#define SQRT5_F         2.236067f

#define SQRT7_L         2.64575131106459059050161575363926l
#define SQRT7           2.64575131106459
#define SQRT7_F         2.645751f


/*
 * =================================================================
 *
 *                          STRUCTS
 *
 * =================================================================
 */





/*
 * =================================================================
 *
 *                          ALGEBRA
 *
 * =================================================================
 */

// quadratic formula. Returns the number of solutions (0 or 2)
int8_t quadform(const double a, const double b, const double c,
        double * x1, double * x2);

int8_t quadformf(const float a, const float b, const float c,
        float * x1, float * x2);

int8_t quadforml(const long double a, const long double b, const long double c,
        long double * x1, long double * x2);




/*
 * =================================================================
 *
 *                          NUMBER THEORY
 *
 * =================================================================
 */

// signum function
int8_t sgn(const double x);
int8_t sgnf(const float x);
int8_t sgnl(const long double x);


/*
 * =================================================================
 *
 *                          STOCHASTIC
 *
 * =================================================================
 */


// means
double mean(const double * const data, const size_t size);
float meanf(const float * const data, const size_t size);
long double meanl(const long double * const data, const size_t size);

double gmean(const double * const data, const size_t size);
float gmeanf(const float * const data, const size_t size);
long double gmeanl(const long double * const data, const size_t size);

double hmean(const double * const data, const size_t size);
float hmeanf(const float * const data, const size_t size);
long double hmeanl(const long double * const data, const size_t size);

// median

// expects a sorted array
double median(const double * const data, const size_t size);
float medianf(const float * const data, const size_t size);
long double medianl(const long double * const data, const size_t size);


// expected value
double expct(const double * const outcomes, 
        const double * const probabilities, 
        const size_t size);

float expctf(const float * const outcomes, 
        const float * const probabilities, 
        const size_t size);

long double expctl(const long double * const outcomes, 
        const long double * const probabilities, 
        const size_t size);
// variance

double var(const double * const outcomes, 
        const double * const probabilities, 
        size_t size);
float varf(const float * const outcomes, 
        const float * const probabilities, 
        size_t size);
long double varl(const long double * const outcomes, 
        const long double * const probabilities, 
        size_t size);

// standard deviation
double stddev(const double * const outcomes, 
        const double * const probabilities, 
        size_t size);

float stddevf(const float * const outcomes, 
        const float * const probabilities, 
        size_t size);

long double stddevl(const long double * const outcomes, 
        const long double * const probabilities, 
        size_t size);




#endif
