# LibEuler - Portable and Performant Mathematics Library
**LibEuler** is a single header C99 library and as such very performant and portable.
It aims to extend the functionality of the standard <math.h> library.

